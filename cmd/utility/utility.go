package utility

import (
	"fmt"
	"math/rand"
)

//Roller dice roller
func Roller(iterations, faces int) (ret int) {
	for i := 1; i <= iterations; i++ {
		rnd := rand.Intn(faces) + 1
		fmt.Println("iteration ", i, "of ", faces, "-faced die :", rnd)
		ret += rnd
	}
	return
}
